/*
 * charstack.hpp
 *
 *  Created on: Feb 21, 2023
 *      Author: student
 */
//#pragma once
#ifndef CHARSTACK_HPP_
#define CHARSTACK_HPP_

class CharStack {
private:
    char *stack;
    int max;
    int size = 0;
public:
    CharStack(int newMax);
    ~CharStack();
    bool push(char c);
    char pop();
    bool isEmpty() const;
};

#endif /* CHARSTACK_HPP_ */
