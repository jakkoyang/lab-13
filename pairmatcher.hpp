/*
 * pairmatcher.hpp
 *
 *  Created on: Feb 21, 2023
 *      Author: student
 */
#include "charstack.hpp"
#include <string>
#ifndef PAIRMATCHER_HPP_
#define PAIRMATCHER_HPP_

class PairMatcher {
private:
    char _openChar, _closeChar;
    CharStack charStack;
public:
    PairMatcher(char openChar, char closeChar);
    bool check(const std::string &testString);
};



#endif /* PAIRMATCHER_HPP_ */
