#include <iostream>
#include "charstack.hpp"
#include "pairmatcher.hpp"

using namespace std;

int main() {
    PairMatcher matcher('(', ')');
    
    string testString = "((())()";
    
    cout << testString << " is " << (matcher.check(testString) ? "valid" : "invalid") << endl;
    return 0;
}
